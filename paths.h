#if !defined(_TYPE_PATHS_H)
#define _TYPE_PATHS_H

#include <stdbool.h>
#include "chess.h"

bool ValidPath(enum Type, Vector2, Vector2);


#endif
