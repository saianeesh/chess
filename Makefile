CC = gcc
CFLAGS = -lraylib -lGL -lpthread -lm -lX11 -ldl
CFLAGS += -Wall -pedantic
CFLAGS += -g
#CFLAGS += -B/usr/local/bin

SRC = $(wildcard *.c)
OBJ = $(patsubst %.c,%.o,${SRC})
HEAD = $(wildcard *.h)
TARGET = chess


.PHONY: clean
.DEFAULT_GOAL: ${TARGET}

${TARGET}: ${OBJ} ${HEAD}
	${CC} ${CFLAGS} -o $@ $^


clean:
	rm *.o ${TARGET}
