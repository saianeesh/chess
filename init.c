#include <stdbool.h>
#include <stdlib.h>
#include "chess.h"

static void
InitKing(Piece *dest)
{
    dest->npoints = 1;
    Vector2 *points;
    if ((points = calloc(dest->npoints, sizeof(Vector2))) == NULL) {
        exit(EXIT_FAILURE);
    }

    if (dest->type == WK) {
        points[0] = (Vector2) {4 * squareWidth, 7 * squareHeight};
    } else {
        points[0] = (Vector2) {4 * squareWidth, 0};
    }
    dest->points = points;
}

void
InitPawn(Piece *dest)
{
    dest->npoints = 8;
    Vector2 *points;
    if ((points = calloc(dest->npoints, sizeof(Vector2))) == NULL) {
        exit(EXIT_FAILURE);
    }

    if (dest->type == WP) {
        for (int i = 0; i < dest->npoints; i++) {
            points[i] = (Vector2) {i * squareWidth, 6 * squareHeight};
        }
    } else {
        for (int i = 0; i < dest->npoints; i++) {
            points[i] = (Vector2) {i * squareWidth, squareHeight};
        }
    }
    dest->points = points;
}

static void
InitBishop(Piece *dest)
{
    dest->npoints = 2;
    Vector2 *points;
    if ((points = calloc(dest->npoints, sizeof(Vector2))) == NULL) {
        exit(EXIT_FAILURE);
    }

    if (dest->type == WB) {
        points[0] = (Vector2) {2 * squareWidth, 7 * squareHeight};
        points[1] = (Vector2) {5 * squareWidth, 7 * squareHeight};
    } else {
        points[0] = (Vector2) {2 * squareWidth, 0};
        points[1] = (Vector2) {5 * squareWidth, 0};
    }
    dest->points = points;
}

static void
InitQueen(Piece *dest)
{
    dest->npoints = 1;
    Vector2 *points;
    if ((points = calloc(dest->npoints, sizeof(Vector2))) == NULL) {
        exit(EXIT_FAILURE);
    }

    if (dest->type == WQ) {
        points[0] = (Vector2) {3 * squareWidth, 7 * squareHeight};
    } else {
        points[0] = (Vector2) {3 * squareWidth, 0};
    }
    dest->points = points;
}

static void
InitRook(Piece *dest)
{
    dest->npoints = 2;
    Vector2 *points;
    if ((points = calloc(dest->npoints, sizeof(Vector2))) == NULL) {
        exit(EXIT_FAILURE);
    }

    if (dest->type == WR) {
        points[0] = (Vector2) {7 * squareWidth, 7 * squareHeight};
        points[1] = (Vector2) {0, 7 * squareHeight};
    } else {
        points[0] = (Vector2) {7 * squareWidth, 0};
        points[1] = (Vector2) {0, 0};
    }
    dest->points = points;
}

static void
InitHorse(Piece *dest)
{
    dest->npoints = 2;
    Vector2 *points;
    if ((points = calloc(dest->npoints, sizeof(Vector2))) == NULL) {
        exit(EXIT_FAILURE);
    }

    if (dest->type == WH) {
        points[0] = (Vector2) {squareWidth, 7 * squareHeight};
        points[1] = (Vector2) {6 * squareWidth, 7 * squareHeight};
    } else {
        points[0] = (Vector2) {squareWidth, 0};
        points[1] = (Vector2) {6 * squareWidth, 0};
    }
    dest->points = points;
}

Piece *
InitPiece(enum Type type, Texture2D texture)
{
    Piece *dest;
    if ((dest = malloc(sizeof(Piece))) == NULL) {
        exit(EXIT_FAILURE);
    }
    dest->texture = texture;
    dest->type = type;
    switch (type) {
        case WK:
        case BK:
            InitKing(dest);
            break;
        case WQ:
        case BQ:
            InitQueen(dest);
            break;
        case WP:
        case BP:
            InitPawn(dest);
            break;
        case WH:
        case BH:
            InitHorse(dest);
            break;
        case WB:
        case BB:
            InitBishop(dest);
            break;
        case WR:
        case BR:
            InitRook(dest);
            break;
    }

    return dest;
}
