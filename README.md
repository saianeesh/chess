# Chess

The program was built using the [raylib library](https://www.raylib.com/).
A very basic program.
Currently only tested on GNU/Linux.

### Compilation
```
make
```
### Install Raylib
To compile the program you need to install the raylib library.
Instructions to build and install can be found [here](https://github.com/raysan5/raylib/wiki/Working-on-GNU-Linux).
