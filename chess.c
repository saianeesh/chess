#include <raylib.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h> 
#include <string.h>

#include "chess.h"
#include "paths.h"
#include "init.h"

Piece *pieces[NUM_PIECES];
const float screenHeight = 640;
const float screenWidth = screenHeight;
const float squareHeight = screenHeight/8;
const float squareWidth = squareHeight;

bool
IsEmpty(Vector2 point)
{
    if (FilterType(FilterWhite, point) ||
            FilterType(FilterBlack, point)) {
        return false;
    }
    return true;
}

Piece *
FindPiece(Vector2 selectedSquare)
{
    Piece *selectedPiece = NULL;
    for (int i = 0; i < NUM_PIECES; i++) {
        Piece *curr = pieces[i];
        for (int point = 0; point < curr->npoints; point++) {
            if (IsEmpty((curr->points)[point])) {
                return selectedPiece;
            }
            if (selectedSquare.x == (curr->points)[point].x &&
                    selectedSquare.y == (curr->points)[point].y) {
                return curr;
            }
        }
    }
    return selectedPiece;
}

bool
SameVector(Vector2 a, Vector2 b)
{
    if (a.x == b.x && a.y == b.y) {
        return true;
    }
    return false;
}

Vector2
SelectedSquare(void)
{
    Vector2 selectedSquare = (Vector2) {0.0f, 0.0f};
    Vector2 mouseClick = GetMousePosition();
    for (float x = 0; x <= screenWidth; x += squareWidth) {
        if (mouseClick.x < x) {
            selectedSquare.x = (x - squareWidth);
            break;
        }
    }
    for (float y = 0; y <= screenHeight; y += squareHeight) {
        if (mouseClick.y < y) {
            selectedSquare.y = (y - squareHeight);
            break;
        }
    }
    return selectedSquare;
}

void
DrawPieces(void)
{
    Piece curr;
    for (int i = WB; i <= BH; i++) {
        curr = *(pieces[i]);
        for (int point = 0; point < curr.npoints; point++) {
            DrawTexture(curr.texture, (curr.points + point)->x, (curr.points + point)->y, WHITE);
        }
    }
}

void
DrawBoard(void)
{
    int count = 0;
    for (float x = 0; x <= screenWidth; x += squareWidth) {
        for (float y = 0; y <= screenHeight; y += squareHeight, count++) {
            DrawRectangle(x, y, squareWidth, squareHeight, ((count%2 == 0) ? BEIGE : DARKBROWN));
        }
    }
}

bool
MovePiece(Vector2 srcPoint, Piece *srcPiece, Vector2 dest)
{
    for (int point = 0; point < srcPiece->npoints; point++) {
        if (srcPoint.x == (srcPiece->points)[point].x &&
                srcPoint.y == (srcPiece->points)[point].y) {
            (srcPiece->points)[point] = dest;
            return true;
        }
    }
    return false;
}


static bool
ValidTypeMove(Vector2 from, Vector2 to, bool *kill)
{
    bool fromWhite = FilterType(FilterWhite, from);
    bool fromBlack = FilterType(FilterBlack, from);
    bool toWhite = FilterType(FilterWhite, to);
    bool toBlack = FilterType(FilterBlack, to);
    if (IsEmpty(to)) {
        return true;
    } else if ((fromWhite && toBlack) || (fromBlack && toWhite)) {
        *kill = true;
        return true;
    } else {
        return false;
    }
}


bool
FilterType(bool (*comp)(enum Type), Vector2 square)
{
    for (int i = 0; i < NUM_PIECES; i++) {
        Piece curr = *pieces[i];
        for (int point = 0; point < curr.npoints; point++) {
            if (square.x == (curr.points)[point].x &&
                    square.y == (curr.points)[point].y && (*comp)(curr.type)) {
                return true;
            }
        }
    }
    return false;
}

bool
FilterWhite(enum Type type)
{
    if (type < WB || type > WH) {
        return false;
    }
    return true;
}

bool
FilterBlack(enum Type type)
{
    if (type < BB || type > BH) {
        return false;
    }
    return true;
}

void
Kill(Vector2 point)
{
    Piece *piece = FindPiece(point);
    for(int i = 0; i < piece->npoints; i++) {
        Vector2 curr = (piece->points)[i];
        if (curr.x == point.x && curr.y == point.y) {
            (piece->points)[i].x = screenWidth;
            (piece->points)[i].y = screenHeight;
            return;
        }
    }
}


int
main(void)
{
    /* Initialise */
    InitWindow(screenWidth, screenHeight, "chess");
    if (!IsWindowReady()) {
        fprintf(stderr, "Window not ready");
        exit(EXIT_FAILURE);
    }
    SetTargetFPS(FPS);

    Image images[12];
    Texture2D textures[NUM_PIECES];
    const char *filenames[NUM_PIECES] = {"white-bishop.png", "white-king.png", "white-queen.png", "white-rook.png", "white-pawn.png", "white-knight.png",
        "black-bishop.png", "black-king.png", "black-queen.png", "black-rook.png", "black-pawn.png", "black-knight.png"};

    {
        /* load Images and Textures */
        char image[50];
        char *folder = "images/";
        for (int type = WB; type <= BH; type++) {
            sprintf(image, "%s%s", folder, filenames[type]);
            images[type] = LoadImage(image);
            memset(image, 0, 50);
            ImageResize(images + type, squareWidth, squareHeight);
            textures[type] = LoadTextureFromImage(images[type]);
            pieces[type] = InitPiece(type, textures[type]);
        }
    }

    while (!WindowShouldClose()) {
        static bool selected = false;
        static bool turn = true; // true : white turn, false : black turn
        static bool kill = false;
        static Vector2 selectedSquare = { (float) 0, (float) 0};
        Piece *selectedPiece;

        BeginDrawing();
            ClearBackground(WHITE);
            DrawBoard();
            DrawPieces();
            if (selected) {
                if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
                    Vector2 toSquare = SelectedSquare();
                    if (ValidTypeMove(selectedSquare, toSquare, &kill)) {
                        selectedPiece = FindPiece(selectedSquare);
                        if (ValidPath(selectedPiece->type, selectedSquare, toSquare)) {
                            if (kill) {
                                Kill(toSquare);
                            }
                            MovePiece(selectedSquare, selectedPiece, toSquare);
                            turn ^= true;
                        }
                        kill = false;
                    }
                    selected = false;
                }
                DrawRectangleLines(selectedSquare.x, selectedSquare.y, squareWidth, squareHeight, RED);
            } else if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
                selectedSquare = SelectedSquare();
                if (IsEmpty(selectedSquare)) {
                    ;
                } else {
                    if ((turn && FilterType(FilterWhite, selectedSquare)) ||
                            (!turn && FilterType(FilterBlack, selectedSquare))) {
                        selectedPiece = FindPiece(selectedSquare);
                        selected = true;
                    } else {
                        ;
                    }
                }
            }
        EndDrawing();
    }

    /* freeing memory */
    for (int i = WB; i <= BH; i++) {
        UnloadTexture(textures[i]);
        UnloadImage(images[i]);
        free(pieces[i]->points);
        free(pieces[i]);
    }

    CloseWindow();
    return 0;
}
