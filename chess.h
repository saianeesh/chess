#if !defined(_CHESS_H)
#define _CHESS_H

#include "raylib.h"

#define MAX(a, b) ((a > b) ? a : b)
#define MIN(a, b) ((a > b) ? b : a)

#define NUM_PIECES 12
#define FPS 30

enum Type {
    WB, WK, WQ, WR, WP, WH,
    BB, BK, BQ, BR, BP, BH,
};


typedef struct Piece {
    Texture2D texture;
    Vector2 *points;
    int npoints;
    enum Type type;
} Piece;


extern Piece *pieces[NUM_PIECES];
extern const float screenHeight;
extern const float screenWidth;
extern const float squareHeight;
extern const float squareWidth;

bool IsEmpty(Vector2);
bool FilterType(bool (*)(enum Type), Vector2);
bool FilterWhite(enum Type);
bool FilterBlack(enum Type);
bool SameVector(Vector2, Vector2);
Piece *FindPiece(Vector2);

#endif
