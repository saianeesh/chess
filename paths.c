#include <stdbool.h>
#include <stdlib.h>

#include "chess.h"
#include "init.h"

static bool
ValidColourMove(Vector2 fromPoint, Vector2 toPoint) {
    bool toBlack = FilterType(FilterBlack, toPoint);
    bool toWhite = FilterType(FilterWhite, toPoint);
    bool fromBlack = FilterType(FilterBlack, fromPoint);
    bool fromWhite = FilterType(FilterWhite, fromPoint);
    if ((fromBlack && toWhite) || (fromWhite && toBlack)) {
        return true;
    }

    return false;
}

static inline double
y(float m, float x, float c)
{
    return ((m * x) + c);
}

static bool
Bishop(Vector2 fromPoint, Vector2 toPoint)
{
    float dy = toPoint.y - fromPoint.y;
    float dx = toPoint.x - fromPoint.x;
    double m = dy / dx;
    if (m == (double) -1 || m == (double) 1) {
        float startx = MIN(fromPoint.x, toPoint.x) + squareWidth;
        float endx = MAX(fromPoint.x, toPoint.x) - squareWidth;
        float c = fromPoint.y - (m * fromPoint.x);
        for (Vector2 tmp; startx <= endx; startx += squareWidth) {
            tmp.x = startx;
            tmp.y = y(m, tmp.x, c);
            if (!IsEmpty(tmp)) {
                return false;
            }
        }
        return true;
    }

    return false;
}

static bool
Rook(Vector2 fromPoint, Vector2 toPoint)
{
    if (fromPoint.x != toPoint.x && fromPoint.y != toPoint.y) {
        return false;
    }

    Vector2 start;
    Vector2 end;

    /* vertical movement */
    if (fromPoint.x == toPoint.x) {
        start.y = MIN(fromPoint.y, toPoint.y) + squareHeight;
        start.x = fromPoint.x;

        end.y = MAX(fromPoint.y, toPoint.y) - squareHeight;
        end.x = toPoint.x;
        for (; start.y <= end.y; start.y += squareHeight) {
            if (!IsEmpty(start)) {
                return false;
            }
        }
    /* horizontal movement */
    } else if (fromPoint.y == toPoint.y) {
        start.x = MIN(fromPoint.x, toPoint.x) + squareWidth;
        start.y = fromPoint.y;

        end.x = MAX(fromPoint.x, toPoint.x) - squareWidth;
        end.y = toPoint.y;
        for (; start.x <= end.x; start.x += squareWidth) {
            if (!IsEmpty(start)) {
                return false;
            }
        }
    }
    
    return true;
}

static bool
Queen(Vector2 fromPoint, Vector2 toPoint)
{
    return (Bishop(fromPoint, toPoint) || Rook(fromPoint, toPoint));
}

static bool
King(Vector2 fromPoint, Vector2 toPoint)
{
    if (toPoint.x > (fromPoint.x + squareHeight) ||
            toPoint.x < (fromPoint.x - squareHeight) ||
            toPoint.y > (fromPoint.y + squareHeight) ||
            toPoint.y < (fromPoint.y - squareHeight)) {
        return false;
    }

    return true;
}

static Vector2 
MoveUp(Vector2 point, int nsquares)
{
    Vector2 newPoint = point;
    if ((point.y - (nsquares * squareHeight)) < 0) {
        return point;
    }
    newPoint.y -= (nsquares * squareHeight);
    return newPoint;
}

static Vector2
MoveDown(Vector2 point, int nsquares)
{
    Vector2 newPoint = point;
    if ((point.y + (nsquares * squareHeight)) > (screenHeight - squareHeight)) {
        return newPoint;
    }
    newPoint.y += (nsquares * squareHeight);
    return newPoint;
}

static Vector2
MoveRight(Vector2 point, int nsquares)
{
    Vector2 newPoint = point;
    if ((point.x + (nsquares * squareWidth)) > (screenWidth - squareWidth)) {
        return newPoint;
    }
    newPoint.x += (nsquares * squareWidth);
    return newPoint;
}

static Vector2
MoveLeft(Vector2 point, int nsquares)
{
    Vector2 newPoint = point;
    if ((point.x - (nsquares * squareWidth)) < 0) {
        return newPoint;
    }
    newPoint.x -= (nsquares * squareWidth);
    return newPoint;
}

static bool
Horse(Vector2 fromPoint, Vector2 toPoint)
{
    Vector2 (*upDown[2])(Vector2, int) = {MoveUp, MoveDown};
    Vector2 (*leftRight[2])(Vector2, int) = {MoveLeft, MoveRight}; 
    Vector2 tmp;
    for (int ud = 0; ud < 2; ud++) {
        tmp = (*upDown[ud])(fromPoint, 2);
        if (SameVector(fromPoint, tmp)) {
            continue;
        }
        for (int lr = 0; lr < 2; lr++) {
            Vector2 final = (*leftRight[lr])(tmp, 1);
            if (!SameVector(final, tmp) && SameVector(final, toPoint)) {
                return true;
            }
        }
    }

    for (int lr = 0; lr < 2; lr++) {
        tmp = (*leftRight[lr])(fromPoint, 2);
        if (SameVector(fromPoint, tmp)) {
            continue;
        }
        for (int ud = 0; ud < 2; ud++) {
            Vector2 final = (*upDown[ud])(tmp, 1);
            if (!SameVector(final, tmp) && SameVector(final, toPoint)) {
                return true;
            }
        }
    }
    return false;
}

static bool
IsStartingPosition(enum Type type, Vector2 position)
{
    Piece initPawn;
    initPawn.type = type;
    InitPawn(&initPawn);
    for (int i = 0; i < initPawn.npoints; i++) {
        if (initPawn.points[i].x == position.x &&
                initPawn.points[i].y == position.y) {
            return true;
        }
    }
    free(initPawn.points);

    return false;
}

static bool
Pawn(Vector2 fromPoint, Vector2 toPoint)
{
    const Piece *curr = FindPiece(fromPoint);
    enum Type type = curr->type;
    int nsquares = (IsStartingPosition(type, fromPoint) ? 2 : 1) + 2;
    Vector2 possibleSquares[nsquares];

    if (FilterWhite(type)) {
        possibleSquares[0] = MoveUp(fromPoint, 1);
        if (IsStartingPosition(type, fromPoint)) {
            possibleSquares[3] = MoveUp(fromPoint, 2);
        }
        if (toPoint.y >= fromPoint.y) {
            return false;
        }
    } else {
        possibleSquares[0] = MoveDown(fromPoint, 1);
        if (IsStartingPosition(type, fromPoint)) {
            possibleSquares[3] = MoveDown(fromPoint, 2);
        }
        if (toPoint.y <= fromPoint.y) {
            return false;
        }
    }
    possibleSquares[1] = MoveRight(possibleSquares[0], 1);
    possibleSquares[2] = MoveLeft(possibleSquares[0], 1);
    for (int i = 0; i < nsquares; i++) {
        /* error checking */
        if (((i == 1 || i == 2) && SameVector(possibleSquares[0], possibleSquares[i]))
                || SameVector(possibleSquares[i], fromPoint)) {
            continue;
        }
        if (SameVector(possibleSquares[i], toPoint)) {
            if (fromPoint.x != toPoint.x) {
                if (ValidColourMove(fromPoint, toPoint)) {
                    ;
                } else {
                    return false;
                }
            } else if (!IsEmpty(possibleSquares[i])) {
                    return false;
            }

            return true;
        }
    }

    return false;
}


bool
ValidPath(enum Type type, Vector2 fromPoint, Vector2 toPoint)
{
    bool valid = false;
    switch(type) {
        case WB:
        case BB:
            valid = Bishop(fromPoint, toPoint);
            break;
        case WK:
        case BK:
            valid = King(fromPoint, toPoint);
            break;
        case WQ:
        case BQ:
            valid = Queen(fromPoint, toPoint);
            break;
        case WR:
        case BR:
            valid = Rook(fromPoint, toPoint);
            break;
        case WP:
        case BP:
            valid = Pawn(fromPoint, toPoint);
            break;
        case WH:
        case BH:
            valid = Horse(fromPoint, toPoint);
            break;
    }

    return valid;
}
