#if !defined(_INIT_H)
#define _INIT_H

#include "chess.h"

Piece *InitPiece(enum Type, Texture2D);
void InitPawn(Piece *);

#endif
